package BrowsePage;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import Home.ExtendedBaseTestCase;
import common.testcases.Configurations;
import homeLuv.Common.ManageFiles.CVSFileManager;
import homeLuv.Common.Utilities.Constants;
import homeLuv.Common.pageObjects.BrowsePage;
import homeLuv.Models.SearchModel;

public class Browse_ZeroSearch extends ExtendedBaseTestCase implements ITest {

	private SearchModel searchObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getSearchDetails")
	public Browse_ZeroSearch(String city, String code, String zip, String market, String county) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));

		searchObj = new SearchModel(city, code, zip, market, county);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		BrowsePage page = PageFactory.initElements(driver, BrowsePage.class);
		try {
			page.Browse_ZeroSearch(searchObj);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	@Override
	public String getTestName() {
		return Constants.Browse_ZeroSearch_Search;

	}
}
