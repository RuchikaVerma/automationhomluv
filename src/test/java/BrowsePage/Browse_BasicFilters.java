package BrowsePage;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Home.ExtendedBaseTestCase;
import Home.ExtentReporterNG;
import common.testcases.Configurations;
import homeLuv.Common.ManageFiles.CVSFileManager;
import homeLuv.Common.Utilities.Constants;
import homeLuv.Common.pageObjects.BrowsePage;
import homeLuv.Models.FiltersModel;
import homeLuv.Models.SearchModel;

@Listeners(ExtentReporterNG.class)
public class Browse_BasicFilters extends ExtendedBaseTestCase implements ITest {

	private FiltersModel filterObj;
	private SearchModel searchObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getFilterDetails")
	public Browse_BasicFilters(String city, String code, String zip, String market, String county, String minprice,
			String maxprice, String minsqft, String maxsqft, String beds, String bath, String buildername,
			String communities, String hometype, String homestatus, String specialoffer, String garages,
			String livingareas, String stories, String masterbedroom) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));

		filterObj = new FiltersModel(minprice, maxprice, minsqft, maxsqft, beds, bath, buildername, communities,
				hometype, homestatus, specialoffer, garages, livingareas, stories, masterbedroom);
		searchObj = new SearchModel(city, code, zip, market, county);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		BrowsePage page = PageFactory.initElements(driver, BrowsePage.class);
		try {
			page.Browse_BasicFilters(filterObj, searchObj);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getTestName() {
		return Constants.Browse_BasicFilters_Search;

	}
}
