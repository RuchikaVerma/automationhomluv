package Home;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import common.testcases.Configurations;
import homeLuv.Common.Utilities.Constants;
import homeLuv.Common.pageObjects.HomePage;

@Listeners(ExtentReporterNG.class)
public class HomePageLinksVideostest extends ExtendedBaseTestCase implements ITest {

	final String URL = "com.newHomeSource.url";

	public HomePageLinksVideostest() {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		HomePage page = PageFactory.initElements(driver, HomePage.class);
		try {
			page.HomePageLinksAndVideos();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getTestName() {
		return Constants.HomePageLinksAndVideosTestCase;
	}
}
