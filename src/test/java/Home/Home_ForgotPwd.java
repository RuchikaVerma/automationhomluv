package Home;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import common.testcases.Configurations;
import homeLuv.Common.ManageFiles.CVSFileManager;
import homeLuv.Common.Utilities.Constants;
import homeLuv.Common.pageObjects.HomePage;
import homeLuv.Models.LoginModel;

@Listeners(ExtentReporterNG.class)
public class Home_ForgotPwd extends ExtendedBaseTestCase implements ITest {
	private LoginModel loginObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getLoginDetails")
	public Home_ForgotPwd(String emailAddress, String password) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		HomePage page = PageFactory.initElements(driver, HomePage.class);
		page.Home_ForgotPwd(loginObj);
	}

	@Override
	public String getTestName() {
		return Constants.Home_ForgotPwdTestCase;
	}
}
