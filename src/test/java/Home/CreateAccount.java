package Home;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import common.testcases.Configurations;
import homeLuv.Common.ManageFiles.CVSFileManager;
import homeLuv.Common.Utilities.Constants;
import homeLuv.Common.pageObjects.HomePage;
import homeLuv.Models.UserModel;

@Listeners(ExtentReporterNG.class)
public class CreateAccount extends ExtendedBaseTestCase implements ITest {
	private UserModel userObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getUserDetails")
	public CreateAccount(String firstName, String lastName, String emailAddress, String password, String zipCode) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		userObj = new UserModel(firstName, lastName, emailAddress, password, zipCode);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		HomePage page = PageFactory.initElements(driver, HomePage.class);
		try {
			page.CreateAccount(userObj);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getTestName() {
		return Constants.CreateAccountTestCase;
	}
}
