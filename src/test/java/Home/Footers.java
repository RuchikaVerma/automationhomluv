package Home;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import common.testcases.Configurations;
import homeLuv.Common.Utilities.Constants;
import homeLuv.Common.pageObjects.BasePageObject;
import homeLuv.Common.pageObjects.HomePage;

@Listeners(ExtentReporterNG.class)
public class Footers extends ExtendedBaseTestCase implements ITest {

	final String URL = "com.newHomeSource.url";

	public Footers() {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		BasePageObject page = PageFactory.initElements(driver, HomePage.class);
		try {
			page.Footers();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getTestName() {
		return Constants.Footers;
	}
}
