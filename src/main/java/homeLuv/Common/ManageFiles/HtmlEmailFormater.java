package homeLuv.Common.ManageFiles;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

import common.testcases.Configurations;

public class HtmlEmailFormater extends common.web.HtmlEmailFormater {
	
	public static String generateMarketVerificationHtmlReport(
			List<LocationBean> locations, String reportTittle, String URL) {
		StringBuilder html = new StringBuilder();
		Collections.sort(locations);
			
		html.append(common.web.HtmlEmailFormater.getHTMLReportHeader(URL));

		if (locations.size() > 0) {// There are errors so we have to write
									// the table with the results

			html.append("<p> There are markets with errors, see details bellow: </p>\n");

			/**** SUMMARY TABLE ******/
			/***** HEADER ****/
			html.append("<table id='execution_summary'>\n");
			html.append("	<thead>\n");
			html.append("		<tr>\n");
			addTableCell(html, "Market Name", false, false);
			addTableCell(html, "State", false, false);
			addTableCell(html, "Market Id", false, false);
			addTableCell(html, "Communities", false, false);
			addTableCell(html, "Homes", false, false);
			addTableCell(html, "Community on Web (Difference)", false, false);
			addTableCell(html, "Home on Web (Difference)", false, false);
			addTableCell(html, "MarketID test", false, false);
			addTableCell(html, "Screenshot Link", false, false);
			html.append("		</tr>\n");
			html.append("	</thead>\n");
			/**** END HEADER *****/

			/**** TABLE BODY ****/
			html.append("	<tbody>\n");

			for (LocationBean location : locations) {
				html.append("		<tr>\n");
				addTableCell(html, location.getCity(), false, false);
				addTableCell(html, location.getStateAbbreviation(), false, false);
				addTableCell(html, "" + location.getMarketID(), false, false);
				addTableCell(html, "" + location.getNumOfCommunities(), false, false);
				addTableCell(html, "" + location.getNumOfHomes(), false, false);

				// Add the test information, and set the background to the cell
				// If failed it is set to red, if the test case pass it stays as white.
				addTableCell(html, location.getTestCommunityNum(), false,
						location.getTestCommunityNum().contains("Fail"));
				addTableCell(html, location.getTestHomeNum(), false,
						location.getTestHomeNum().contains("Fail"));
				addTableCell(html, location.getTestMarketId(), false,
						location.getTestMarketId().contains("Fail"));
				
				// Add the column with the Url to the failure screenshot
				Properties prop = Configurations.getPropertiesManager();
				String ftpServer = prop.getProperty("local_ftp_server");
				String buildFolder = prop.getProperty("outputFolder");
				int endExecutionFolder = buildFolder.indexOf("/");
				String outFolder = buildFolder.substring(endExecutionFolder, buildFolder.length());
				String screenshotName = "MVA_failure_location_" + location.getCity() + "_" + location.getStateAbbreviation();
				String screenshotLink = "<a href='ftp://" + ftpServer + "/" + outFolder + "/FailureScreenshots/screenshot-" + screenshotName + ".jpeg'>Failure's Screenshot</a>";
				
				addTableCell(html, screenshotLink, false,
						location.getTestMarketId().contains("Fail"));

				html.append("		</tr>\n");
			}

			html.append("	</tbody>\n");
			html.append("</table>\n");

		} else {
			html.append("<p id='noErrorsFound'> No errors were found. All the markets have the right number of communities and homes. </p>\n");
		}
		
		return html.toString();
	}
}
