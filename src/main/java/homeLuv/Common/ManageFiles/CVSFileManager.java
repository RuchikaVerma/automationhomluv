package homeLuv.Common.ManageFiles;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.DataProvider;

import common.testcases.Configurations;

public class CVSFileManager {

	private static List<LocationBean> locations = new ArrayList<LocationBean>();
	final static String signInFileProperty = "SignInFileName";
	final static String userDetailsFileProperty = "UserDetailsFileName";
	final static String FiltersProperty = "FiltersFileName";
	final static String savedListingsFileProperty = "SavedListingsFileName";
	final static String SearchFileProperty = "SearchFileName";;

	@DataProvider
	public static Iterator<Object[]> getSearchDetails() {
		List<Object[]> userData = new ArrayList<Object[]>();
		String dataRow = "";
		try {

			BufferedReader buffer = getBufferReaderOfFile(SearchFileProperty);
			dataRow = buffer.readLine();

			while (dataRow != null) {
				String[] dataArray = dataRow.split(",");
				Object[] currentData = new Object[] { dataArray[0].trim(), // city
						dataArray[1].trim(), // code
						dataArray[2].trim(), // zip
						dataArray[3].trim(), // market
						dataArray[4].trim() // county

				};

				dataRow = buffer.readLine();
				userData.add(currentData);
			}
			buffer.close();
		} catch (FileNotFoundException e) {
			Assert.fail("The file with the expected User Login Details was not found.");
		} catch (IOException e) {
			Assert.fail("There was an error reading the file.");
		}
		return userData.iterator();
	}

	@DataProvider
	public static Iterator<Object[]> getSavedListingsDetails() {
		List<Object[]> userData = new ArrayList<Object[]>();
		String dataRow = "";
		try {

			BufferedReader buffer = getBufferReaderOfFile(savedListingsFileProperty);
			dataRow = buffer.readLine();

			while (dataRow != null) {
				String[] dataArray = dataRow.split(",");
				Object[] currentData = new Object[] { dataArray[0].trim(), // Email
						dataArray[1].trim(), // Password
						dataArray[2].trim(), // FirstName
						dataArray[3].trim(), // LastName
						dataArray[4].trim(), // Email OF Client
						dataArray[5].trim(), // City
						dataArray[6].trim(), // State Code
						dataArray[7].trim() };

				dataRow = buffer.readLine();
				userData.add(currentData);
			}
			buffer.close();
		} catch (FileNotFoundException e) {
			Assert.fail("The file with the expected User Login Details was not found.");
		} catch (IOException e) {
			Assert.fail("There was an error reading the file.");
		}
		return userData.iterator();
	}

	@DataProvider
	public static Iterator<Object[]> getFilterDetails() {
		List<Object[]> userData = new ArrayList<Object[]>();
		String dataRow = "";
		try {

			BufferedReader buffer = getBufferReaderOfFile(FiltersProperty);
			dataRow = buffer.readLine();

			while (dataRow != null) {
				String[] dataArray = dataRow.split(",");
				Object[] currentData = new Object[] { dataArray[0].trim(), // city
						dataArray[1].trim(), // code
						dataArray[2].trim(), // zip
						dataArray[3].trim(), // market
						dataArray[4].trim(), // county
						dataArray[5].trim(), // MinPrice
						dataArray[6].trim(), // Max Price
						dataArray[7].trim(), // Min SqFt
						dataArray[8].trim(), // Max SqFt
						dataArray[9].trim(), // Beds
						dataArray[10].trim(), // Bath
						dataArray[11].trim(), // BuilderName
						dataArray[12].trim(), // Communities
						dataArray[13].trim(), // HomeType
						dataArray[14].trim(), // HomeStatus
						dataArray[15].trim(), // SpecialOffer
						dataArray[16].trim(), // Garages
						dataArray[17].trim(), // LivingAreas
						dataArray[18].trim(), // Stories
						dataArray[19].trim()// MasterBedroom
				};

				dataRow = buffer.readLine();
				userData.add(currentData);
			}
			buffer.close();
		} catch (FileNotFoundException e) {
			Assert.fail("The file with the expected User Login Details was not found.");
		} catch (IOException e) {
			Assert.fail("There was an error reading the file.");
		}
		return userData.iterator();
	}

	@DataProvider
	public static Iterator<Object[]> getLoginDetails() {
		List<Object[]> userData = new ArrayList<Object[]>();
		String dataRow = "";
		try {

			BufferedReader buffer = getBufferReaderOfFile(signInFileProperty);
			dataRow = buffer.readLine();

			while (dataRow != null) {
				String[] dataArray = dataRow.split(",");
				Object[] currentData = new Object[] { dataArray[0].trim(), // Email
						dataArray[1].trim()// Password
				};

				dataRow = buffer.readLine();
				userData.add(currentData);
			}

			buffer.close();
		} catch (FileNotFoundException e) {
			Assert.fail("The file with the expected User Login Details was not found.");
		} catch (IOException e) {
			Assert.fail("There was an error reading the file.");
		}
		return userData.iterator();
	}

	@DataProvider
	public static Iterator<Object[]> getUserDetails() {
		List<Object[]> users = new ArrayList<Object[]>();
		String dataRow = "";
		try {

			BufferedReader buffer = getBufferReaderOfFile(userDetailsFileProperty);
			dataRow = buffer.readLine();

			while (dataRow != null) {
				String[] dataArray = dataRow.split(",");
				Object[] currentUser = new Object[] { dataArray[0].trim(), // EmailAddress
						dataArray[1].trim(), // FirstName
						dataArray[2].trim(), // LastName
						dataArray[3].trim(), // ZipCode
						dataArray[4].trim(), // Password

				};

				dataRow = buffer.readLine();
				users.add(currentUser);
			}
		} catch (FileNotFoundException e) {
			Assert.fail("The file with the expected user details was not found.");
		} catch (IOException e) {
			Assert.fail("There was an error reading the file.");
		}
		return users.iterator();
	}

	public void generateCSVFileWithExecutionStatus(LocationBean newLocation, String fileName) {
		try {

			locations.add(newLocation);

			Properties prop = Configurations.getPropertiesManager();

			BufferedWriter writer = new BufferedWriter(
					new FileWriter(prop.getProperty("outputFolder") + "/" + fileName + ".csv"));
			boolean isFirstLine = true;// Use to writhe the offset numbers that
										// are required only the first line

			// When a test case is restarted it can be added to the list twice,
			// So I need to delete the duplicates
			List<LocationBean> dedupped = new ArrayList<LocationBean>(new LinkedHashSet<LocationBean>(locations));

			for (LocationBean location : dedupped) {
				String currentCommunitiesNum = location.getCurrentCommunitiesNum() + "("
						+ (location.getCurrentCommunitiesNum() - location.getNumOfCommunities()) + ")";
				String currentHomesNum = location.getCurrentHomeNum() + "("
						+ (location.getCurrentHomeNum() - location.getNumOfHomes()) + ")";
				String newLine = location.getCity() + "," + location.getStateAbbreviation() + ","
						+ location.getMarketID() + "," + location.getNumOfCommunities() + "," + location.getNumOfHomes()
						+ "," + currentCommunitiesNum + "," + currentHomesNum + "," + location.getCurrentMarketId()
						+ "," + location.getTestCommunityNum() + "," + location.getTestHomeNum() + ","
						+ location.getTestMarketId();
				if (isFirstLine) {

					ClassLoader loader = Thread.currentThread().getContextClassLoader();
					InputStream stream = loader.getResourceAsStream("custom.properties");
					prop.load(stream);
					newLine += ",," + prop.getProperty("offset_CommunityNumber") + ","
							+ prop.getProperty("offset_HomesNumber");
					isFirstLine = false;
				}
				writer.write(newLine);
				writer.newLine();
			}

			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("There is an error writing the locations with errors. Details: " + e.getMessage());
		}

	}

	public void generateCSVForRerun(LocationBean newLocation, String fileName) {
		try {
			if (newLocation.isFailedEvaluation()) {
				locations.add(newLocation);
			}
			Properties prop = Configurations.getPropertiesManager();
			String folderLocation = prop.getProperty("outputFolder");
			BufferedWriter writer = new BufferedWriter(new FileWriter(folderLocation + "/" + fileName + ".csv"));
			for (LocationBean location : locations) {
				String newLine = location.getCity() + "," + location.getStateAbbreviation() + ","
						+ location.getMarketID() + "," + location.getNumOfCommunities() + ","
						+ location.getNumOfHomes();
				writer.write(newLine);
				writer.newLine();
			}

			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("There is an error writing the csv for rerun the failed locations.");
		}

	}

	private static BufferedReader getBufferReaderOfFile(String propertyName) {
		BufferedReader buffer = null;
		InputStream inputString = null;

		try {
			// Read the path where the .csv was stored
			Properties prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			try {
				InputStream stream = loader.getResourceAsStream("fileName.properties");
				prop.load(stream);
				System.out.println(stream);
			} catch (MalformedURLException e) {
				Assert.fail("Error while streaming fileName.properties." + e.getCause());
			} catch (UnknownHostException e) {
				Assert.fail("Error while streaming fileName.properties." + e.getCause());
			} catch (IOException e) {
				Assert.fail("Error while streaming fileName.properties." + e.getCause());
			}
			if (Boolean.parseBoolean(prop.getProperty("use_local_file"))) {
				System.out.println(prop.getProperty(propertyName));
				buffer = new BufferedReader(new FileReader("src/main/resources/" + prop.getProperty(propertyName)));
			} else {
				System.out.println("url file + " + prop.getProperty("url_of_file"));

				URL urlOfFile = new URL(prop.getProperty("ulr_of_file") + prop.getProperty(propertyName));
				URLConnection connection = urlOfFile.openConnection();

				inputString = connection.getInputStream();
				InputStreamReader reader = new InputStreamReader(inputString);
				buffer = new BufferedReader(reader);
			}

		} catch (MalformedURLException e) {
			Assert.fail("There was an error reading the file with the expected details." + e.getCause());
		} catch (UnknownHostException e) {
			Assert.fail("There was an error reading the file with the expected details." + e.getCause());
		} catch (IOException e) {
			Assert.fail("There was an error reading the file with the expected details." + e.getCause());
		}
		return buffer;
	}

	public static Properties getFileNameProperties() {
		Properties prop = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			InputStream stream = loader.getResourceAsStream("fileName.properties");
			prop.load(stream);
			return prop;
		} catch (MalformedURLException e) {
			Assert.fail("Error while streaming fileName.properties." + e.getCause());
			return null;
		} catch (UnknownHostException e) {
			Assert.fail("Error while streaming fileName.properties." + e.getCause());
			return null;
		} catch (IOException e) {
			Assert.fail("Error while streaming fileName.properties." + e.getCause());
			return null;
		}
	}

}
