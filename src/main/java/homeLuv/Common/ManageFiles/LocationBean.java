package homeLuv.Common.ManageFiles;


public class LocationBean implements Comparable<LocationBean>{
	private String city;
	private String stateAbbreviation;
	private int marketID;
	private int numOfCommunities;
	private int numOfHomes;
	private String testCommunityNum;//Fail or Success
	private String testHomeNum;		//Fail or Success
	private String testMarketId;	//Fail or Success too
	private int currentCommunitiesNum;
	private int currentHomeNum;
	private int currentMarketId;
	private String marketAreaName;
	private boolean failEvaluation;
	private boolean tested;

	public LocationBean(String city, String stateAbbreviation, int marketID,
			int numOfCommunities, int numOfHomes, int offsetCommunities,
			int offsetHomes, String testCommunityNum, String testHomeNum, String testMarketId, int currentCommunitiesNum,
			int currentHomeNum, int currentMarketId, String marketAreaName, boolean errorOnPage, boolean tested){
		this.city = city;
		this.stateAbbreviation = stateAbbreviation;
		this.marketID = marketID;
		this.numOfCommunities = numOfCommunities;
		this.numOfHomes = numOfHomes;
		this.testCommunityNum = testCommunityNum;
		this.setTestHomeNum(testHomeNum);
		this.testMarketId = testMarketId;
		this.currentCommunitiesNum = currentCommunitiesNum;
		this.currentHomeNum = currentHomeNum;
		this.currentMarketId = currentMarketId;
		this.marketAreaName = marketAreaName;
		this.failEvaluation = errorOnPage;
		this.setTested(tested);
	}
	
	public LocationBean(){
		this.city = "";
		this.stateAbbreviation = "";
		this.marketID = 0;
		this.numOfCommunities = 0;
		this.numOfHomes = 0;
		this.testCommunityNum = "Pass";
		this.setTestHomeNum("Pass");
		this.testMarketId = "Pass";
		this.currentCommunitiesNum = 0;
		this.currentHomeNum = 0;
		this.currentMarketId = 0;
		this.marketAreaName = "";
		this.failEvaluation = false;
		this.setTested(false);
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStateAbbreviation() {
		return stateAbbreviation;
	}
	public void setStateAbbreviation(String stateAbbreviation) {
		this.stateAbbreviation = stateAbbreviation;
	}
	public int getMarketID() {
		return marketID;
	}
	public void setMarketID(int marketID) {
		this.marketID = marketID;
	}
	public int getNumOfCommunities() {
		return numOfCommunities;
	}
	public void setNumOfCommunities(int numOfCommunities) {
		this.numOfCommunities = numOfCommunities;
	}
	public int getNumOfHomes() {
		return numOfHomes;
	}
	public void setNumOfHomes(int numOfHomes) {
		this.numOfHomes = numOfHomes;
	}
	public int getCurrentCommunitiesNum() {
		return currentCommunitiesNum;
	}
	public void setCurrentCommunitiesNum(int currentCommunitiesNum) {
		this.currentCommunitiesNum = currentCommunitiesNum;
	}
	public int getCurrentHomeNum() {
		return currentHomeNum;
	}
	public void setCurrentHomeNum(int currentHomeNum) {
		this.currentHomeNum = currentHomeNum;
	}
	
	public String getTestCommunityNum() {
		return testCommunityNum;
	}

	public void setTestCommunityNum(String testCommunityNum) {
		this.testCommunityNum = testCommunityNum;
	}

	public String getTestMarketId() {
		return testMarketId;
	}

	public void setTestMarketId(String testMarketId) {
		this.testMarketId = testMarketId;
	}
	public int getCurrentMarketId() {
		return currentMarketId;
	}

	public void setCurrentMarketId(int currentMarketId) {
		this.currentMarketId = currentMarketId;
	}

	public String getMarketAreaName() {
		return marketAreaName;
	}

	public void setMarketAreaName(String marketAreaName) {
		this.marketAreaName = marketAreaName;
	}
	
	public boolean isFailedEvaluation() {
		return failEvaluation;
	}

	public void setFailedEvaluation(boolean executionStatus) {
		this.failEvaluation = executionStatus;
	}

	public boolean isTested() {
		return tested;
	}

	public void setTested(boolean tested) {
		this.tested = tested;
	}

	public String getTestHomeNum() {
		return testHomeNum;
	}

	public void setTestHomeNum(String testHomeNum) {
		this.testHomeNum = testHomeNum;
	}

	public int compareTo(LocationBean location) {
		return Comparators.CITY.compare(this, location);
	}
}

