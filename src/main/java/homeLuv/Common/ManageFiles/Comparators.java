package homeLuv.Common.ManageFiles;

import java.util.Comparator;

public class Comparators {
	public static Comparator<LocationBean> CITY = new Comparator<LocationBean>() {
		public int compare(LocationBean location1, LocationBean location2){
			return location1.getCity().compareTo(location2.getCity());
		}
	};
}
	
