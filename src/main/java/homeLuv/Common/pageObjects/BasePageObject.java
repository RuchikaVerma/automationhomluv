package homeLuv.Common.pageObjects;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import common.web.BasePage;
import homeLuv.Models.LoginModel;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class BasePageObject extends BasePage {

	public BasePageObject(WebDriver driver) {
		super(driver);

	}

	@FindBy(xpath = ".//*[@id='signinbutton']")
	protected WebElement SigninLink; // signin link from header
	@FindBy(xpath = ".//*[@id='login-modal-content']/div[2]/form/div[1]/div/input")
	protected WebElement emailField;
	@FindBy(xpath = ".//*[@id='loginModal") // SignIn modal link from header
	WebElement loginButton; // signin link
	@FindBy(xpath = ".//*[@id='login-modal-content']/div[2]/form/div[2]/div/input") // Password
	// "signin-signup" // Textbox
	protected WebElement passwordfield;
	@FindBy(xpath = ".//*[@class='form-row hlvs-signInUp-btn']//*[text()='Sign In']")
	protected WebElement submitButton; // sigin btn

	@FindBy(xpath = ".//*[@id='headerContent_']/ul/li[1]")
	protected WebElement Signuplink; //

	@FindBy(xpath = ".//*[@id='headerContent_']/ul/li[2]")
	protected WebElement Signinlink; //

	@FindBy(css = "#commonFooter_>p")
	public WebElement FooterTxt;
	@FindBy(xpath = ".//*[@id='myluvWrap']/header/div/div[1]/a/img")
	public WebElement HomeImage;

	@FindBy(xpath = ".//*[@id='myluvWrap']/header/div/div[1]/a/img")
	public WebElement SiginDropdown;

	// @FindBy(xpath = ".//*[@id='myluvWrap']/header/div/div[1]/a/img")
	// public WebElement HomeImage;

	// Login Method
	protected void login(LoginModel loginObj) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='myluvWrap']/header/div")));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='headerContent_']/ul/li[1]")));
			Thread.sleep(3000);
			Signinlink.click();
			Thread.sleep(3000);
			emailField.sendKeys(loginObj.EmailAddress);
			passwordfield.sendKeys(loginObj.Password);
			submitButton.click();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Login Denied! Here is the cause of exception: " + e.toString());
		}

	}

	protected void Signup_login(LoginModel loginObj) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='myluvWrap']/header/div")));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='headerContent_']/ul/li[1]")));
			Thread.sleep(3000);
			Signuplink.click();

			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Sign in")))
					.click();
			Thread.sleep(3000);
			emailField.sendKeys(loginObj.EmailAddress);
			passwordfield.sendKeys(loginObj.Password);
			submitButton.click();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Login Denied! Here is the cause of exception: " + e.toString());
		}

	}

	// Taking screenshots
	public void screenshot() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
				.takeScreenshot(driver);
		try {

			ImageIO.write(screenshot.getImage(), "PNG", new File("src\\test\\resources\\Images" + driver.getTitle()
					+ formater.format(calendar.getTime()) + "_.png"));

		} catch (IOException e) {
			Assert.fail(e.getMessage());
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	// Footers Links(Site Index, Privacy Policy and Terms&Conditions)testing
	public void Footers() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,3000)");
		driver.findElement(By.linkText("Site Index")).click();
		System.out.println("Sitemap url is   " + driver.getCurrentUrl());

		try {
			Thread.sleep(5000);
		} catch (Exception e) {
			// TODO: handle exception
		}

		LinksImagesVideosVerification.validateAllLinksOnPage(driver);
		LinksImagesVideosVerification.validateInvalidImages(driver);

		HomeImage.click();
		js.executeScript("window.scrollBy(0,3000)");
		driver.findElement(By.linkText("Privacy Policy")).click();
		System.out.println("Privacy and Policy url is   " + driver.getCurrentUrl());
		Thread.sleep(3000);
		LinksImagesVideosVerification.validateAllLinksOnPage(driver);
		LinksImagesVideosVerification.validateInvalidImages(driver);
		HomeImage.click();

		js.executeScript("window.scrollBy(0,3000)");
		driver.findElement(By.linkText("Terms & Conditions")).click();
		System.out.println("Terms and conditions URL is   " + driver.getCurrentUrl());
		Thread.sleep(2000);
		LinksImagesVideosVerification.validateAllLinksOnPage(driver);
		LinksImagesVideosVerification.validateInvalidImages(driver);
		HomeImage.click();

		js.executeScript("window.scrollBy(0,3000)");
		System.out.println("The footer text at home page is :- " + FooterTxt.getText());

	}
}
