package homeLuv.Common.pageObjects;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LinksImagesVideosVerification {

	private static int invalidLinksCount;
	private static int invalidImageCount;

	public static void validateInvalidImages(WebDriver driver) {
		try {
			invalidImageCount = 0;
			List<WebElement> imagesList = driver.findElements(By.tagName("img"));
			Set<String> linkedHashSet = new LinkedHashSet<>();

			for (WebElement imgElement : imagesList) {
				String url = imgElement.getAttribute("src");
				linkedHashSet.add(url);
			}

			System.out.println("Total no. of images are " + imagesList.size());
			System.out.println("Total no. of unique images are " + imagesList.size());

			for (String imgSrc : linkedHashSet) {
				if (imgSrc != null) {
					System.out.println("URL : " + imgSrc);
					verifyURLStatus(imgSrc);
				}
			}
			System.out.println("Total no. of invalid images are " + invalidImageCount);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public static void validateAllLinksOnPage(WebDriver driver) {

		try {
			invalidLinksCount = 0;
			Set<String> linkedHashSet = new LinkedHashSet<>();
			List<WebElement> anchorTagsList = driver.findElements(By.tagName("a"));
			for (WebElement anchorTagElement : anchorTagsList) {
				String url = anchorTagElement.getAttribute("href");
				linkedHashSet.add(url);
			}

			System.out.println("Total no. of links are " + anchorTagsList.size());
			System.out.println("Total no. of unique links are " + linkedHashSet.size());

			for (String urlObtained : linkedHashSet) {
				if (urlObtained != null && !urlObtained.contains("javascript") && !urlObtained.contains("linkedin")
						&& urlObtained.contains("http")) {
					System.out.println("URL : " + urlObtained);
					verifyURLStatus(urlObtained);
				}
			}

			System.out.println("Total no. of invalid links are " + invalidLinksCount);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public static void verifyURLStatus(String URL) {

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(URL);
		try {
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() != 200) {
				invalidLinksCount++;
				System.out.println("Invalid link url's are " + URL + " and status code for that is "
						+ response.getStatusLine().getStatusCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String isLinkBroken(String url) throws Exception {
		String response = "";
		URL httpUrl = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) httpUrl.openConnection();
		try {
			connection.connect();
			response = connection.getResponseMessage();
			connection.disconnect();
			return response;
		}

		catch (Exception exp) {
			return exp.getMessage();
		}
	}

}
