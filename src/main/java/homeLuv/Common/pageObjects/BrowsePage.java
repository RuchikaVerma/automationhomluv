package homeLuv.Common.pageObjects;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import homeLuv.Models.FiltersModel;
import homeLuv.Models.SearchModel;

public class BrowsePage extends BasePageObject {

	@FindBy(id = "searchTextbox")
	public WebElement SearchTextbox;

	@FindBy(xpath = ".//*[@id='searchbutton']")
	public WebElement SearchButton;

	@FindBy(xpath = ".//*[@id='listdata']/div[2]/img")
	public WebElement Loading;

	@FindBy(xpath = ".//*[@id='mainDiv']/section/div[1]/div")
	public WebElement HeroImage;

	@FindBy(xpath = ".//*[@id='imageCatSlider']")
	public WebElement CategorySlider;

	@FindBy(xpath = ".//*[@id='imageCatSlider']/li[1]/a")
	public WebElement BrowseKitchen;

	@FindBy(xpath = ".//*[@id='mainDiv']/section/div[1]/div[2]/ul/li[2]/div[1]")
	public WebElement HomeBathrooms;

	@FindBy(xpath = ".//*[@id='imageCatSlider']/li[2]/a")
	public WebElement BrowseBaths;

	@FindBy(xpath = ".//*[@id='imageCatSlider']/li[3]/a")
	public WebElement BrowseBeds;

	@FindBy(xpath = ".//*[@id='imageCatSlider']/li[4]/a")
	public WebElement LivingArea;

	@FindBy(xpath = ".//*[@id='imageCatSlider']/li[5]/a")
	public WebElement Interiors;

	@FindBy(xpath = ".//*[@id='imageCatSlider']/li[6]/a")
	public WebElement Exteriors;

	@FindBy(css = ".hlvs-browser-breadcrumb.clearfix>ul>li:last-child>a")
	public WebElement BreadCrumb;

	@FindBy(xpath = "(//button[contains(text(),'Got It')])[1]")
	public WebElement Browse_GotIt_btn;

	@FindBy(xpath = ".//*[@id='hlvs-filter']/div[1]/div[5]")
	public WebElement Count;

	@FindBy(css = ".hlvs-no-record>h3")
	public WebElement ZeroSearchRecord;

	@FindBy(xpath = "(.//label[text()='Price']/../div/div/span)[1]")
	public WebElement Priceslider;
	@FindBy(xpath = "(.//label[text()='Square Feet']/../div/div/span)[1]")
	public WebElement SqFtslider;
	@FindBy(xpath = "(.//input[@value='1'][@name='beds'])[1]/..")
	public WebElement Beds;
	@FindBy(xpath = "(.//input[@value='1'][@name='beds'])[2]/..")
	public WebElement Bath;

	String Error = "";

	public BrowsePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void search(SearchModel searchObj) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		SearchTextbox.clear();
		SearchTextbox.sendKeys(searchObj.City + ", " + searchObj.Code);
		SearchButton.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));
	}

	// Browse Page Room Category

	public void Browse_RoomCategory(SearchModel searchObj) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		search(searchObj);
		System.out.println(driver.getCurrentUrl());
		Thread.sleep(5000);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		try {

			WebElement element = (new WebDriverWait(driver, 10)).until(
					ExpectedConditions.elementToBeClickable(By.xpath("(.//button[contains(text(),'Got It')])[2]")));
			element.click();
			Thread.sleep(5000);
		} catch (Exception e) {
			Error += "issue is :" + e.toString();
		}
		Thread.sleep(5000);
		if (BrowseKitchen.isEnabled()) {
			System.out.println("Kitchen is selected by default");
		}

		else {

			System.out.println("Kitchen is not selected by default");
		}
		String KitchenBreadcrumb = BreadCrumb.getText();
		System.out.println(" Kitchen Breadcrumb text is " + KitchenBreadcrumb);
		Assert.assertTrue(driver.getCurrentUrl().contains("kitchen"), "Kitchen room category verification failed");
		BrowseBaths.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		WebElement element2 = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//button[contains(text(),'Got It')])[1]")));
		element2.click();
		// Browse_GotIt_btn.click();
		Thread.sleep(5000);
		System.out.println(driver.getCurrentUrl());
		if (BrowseBaths.isEnabled()) {
			System.out.println("Bathroom is selected ");
		}

		else {

			System.out.println("Bathroom is not selected ");
		}
		String BathBreadcrumb = BreadCrumb.getText();
		System.out.println("Bathroom breadcrumb text is " + BathBreadcrumb);
		Assert.assertTrue(driver.getCurrentUrl().contains("bathroom"), "Bathroom room category verification failed");
		BrowseBeds.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		System.out.println(driver.getCurrentUrl());
		if (BrowseBeds.isEnabled()) {
			System.out.println("Bedroom is selected ");
		}

		else {

			System.out.println("Bedroom is not selected ");
		}
		String BedBreadcrumb = BreadCrumb.getText();
		System.out.println("Bedroom breadcrumb text is " + BedBreadcrumb);
		Assert.assertTrue(driver.getCurrentUrl().contains("bedroom"), "Bedroom room category verification failed");
		LivingArea.click();
		System.out.println(driver.getCurrentUrl());
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		if (LivingArea.isEnabled()) {
			System.out.println("LIVING is selected");
		}

		else {

			System.out.println("Living is not selected");
		}
		String LivingBreadcrumb = BreadCrumb.getText();
		System.out.println("Living breadcrumb text is " + LivingBreadcrumb);
		Assert.assertTrue(driver.getCurrentUrl().contains("living"), "Living room category verification failed");

		Interiors.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		System.out.println(driver.getCurrentUrl());

		if (Interiors.isEnabled()) {
			System.out.println("interiors is selected");
		}

		else {

			System.out.println("Interiors is not selected ");
		}
		String InteriorBreadcrumb = BreadCrumb.getText();
		System.out.println("Interior breadcrumb text is " + InteriorBreadcrumb);
		Assert.assertTrue(driver.getCurrentUrl().contains("interior"), "Interior room category verification failed");

		Exteriors.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		System.out.println(driver.getCurrentUrl());

		if (Exteriors.isEnabled()) {
			System.out.println("exteriors is selected ");
		}

		else {

			System.out.println("Exteriors is not selected ");
		}
		String ExteriorBreadcrumb = BreadCrumb.getText();
		System.out.println("Exterior breadcrumb text is " + ExteriorBreadcrumb);
		Assert.assertTrue(driver.getCurrentUrl().contains("new"), "Exterior room category verification failed");

	}

	// BrowsePage search Location with respect of API testing
	public void Browse_SearchLocation(SearchModel searchObj) throws InterruptedException, JSONException {

		int countcomm = 0;
		int counthome = 0;
		try {

			// Passing API Url to check for api data so that we can match it
			URL url = new URL(
					"http://hlapi.newhomesource.com/Api/v2/search/homes?PartnerId=9383&marketid=269&radius=25&OriginLat=30.266899&OriginLng=-97.742798&pagesize=60&page=1&sortBy=Random&minImgWidth=400&minImgHeight=300&ExcludeDescription=1&ExcludeSummary=1&ExcludeBasiCommunities=1&ExcludeBasicListings=1&ExcludeSchoolDistricts=1&StateName=TX&sflow=500&sfhigh=10000&city=Austin&State=TX&imgType=ELE%2CINT&imgCat=Breakfast%2CKitchen%2CPantry&imgcolortype=TrueColor&Client=HomLuv");
			// Making connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			// Checking for the response
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("HTTP error code : " + conn.getResponseCode());
			}
			Scanner scan = new Scanner(url.openStream());
			String entireResponse = new String();
			while (scan.hasNext())
				entireResponse += scan.nextLine();
			System.out.println("Response : " + entireResponse);
			scan.close();
			JSONObject obj = new JSONObject(entireResponse);
			JSONObject count1 = obj.getJSONObject("ResultCounts");
			countcomm = count1.getInt("CommCount");
			counthome = count1.getInt("CommAllHomeCount");

			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		WebDriverWait wait = new WebDriverWait(driver, 10);

		search(searchObj);
		System.out.println(driver.getCurrentUrl());
		Thread.sleep(5000);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		try {

			WebElement element = (new WebDriverWait(driver, 10)).until(
					ExpectedConditions.elementToBeClickable(By.xpath("(.//button[contains(text(),'Got It')])[2]")));
			element.click();
			Thread.sleep(5000);
		} catch (Exception e) {
			Error += "issue is :" + e.toString();
		}
		Thread.sleep(5000);
		String Count_CommHomes_Kitchen = Count.getText();
		String[] parts = Count_CommHomes_Kitchen.split(" ");
		String part1 = parts[0]; // 004
		System.out.println(part1);
		int resultcomm = Integer.parseInt(part1);
		String part2 = parts[1]; // 034556
		String part3 = parts[2]; // 034556
		String part4 = parts[3]; // 034556
		int resulthome = Integer.parseInt(part4);
		String part5 = parts[4]; // 034556
		try {
			if (countcomm == resultcomm) {

				System.out.println("community count matches  for city search and kitchen category");
			} else {
				System.out.println("community count does not matches  for city search and kitchen category");
			}
			Assert.assertTrue(countcomm == resultcomm, "community count does not match for kitchen and city search");

			if (counthome == resulthome) {

				System.out.println("home count matches for city search and kitchen category");
			} else {
				System.out.println("home count does not matches  for city search and kitchen category");
			}
			Assert.assertTrue(counthome == resulthome, "home count does not match for kitchen and city search");
		} catch (Exception ee) {
			Assert.fail(
					"Test Failed - Community count and home count does not matches  for city search and kitchen category"
							+ ee.toString());
		}

		if (BrowseKitchen.isEnabled()) {
			System.out.println("Kitchen is selected by default");
		}

		else {

			System.out.println("Kitchen is not selected by default");
		}

		Assert.assertTrue(driver.getCurrentUrl().contains("kitchen"),
				"Kitchen room category verification with location as city failed");

		BrowseBaths.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		WebElement element2 = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//button[contains(text(),'Got It')])[1]")));
		element2.click();
		try {

			// Passing API Url to check for api data so that we can match it
			URL url = new URL(
					"http://hlapi.newhomesource.com/Api/v2/search/homes?PartnerId=9383&marketid=275&radius=25&OriginLat=33.096800&OriginLng=-96.634100&pagesize=60&page=1&sortBy=Random&minImgWidth=400&minImgHeight=300&ExcludeDescription=1&ExcludeSummary=1&ExcludeBasiCommunities=1&ExcludeBasicListings=1&ExcludeSchoolDistricts=1&StateName=TX&sflow=500&sfhigh=10000&postalCode=75002&imgType=ELE%2CINT&imgCat=Bathroom&imgcolortype=TrueColor&Client=HomLuv");
			// Making connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			// Checking for the response
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("HTTP error code : " + conn.getResponseCode());
			}
			Scanner scan = new Scanner(url.openStream());
			String entireResponse = new String();
			while (scan.hasNext())
				entireResponse += scan.nextLine();
			System.out.println("Response : " + entireResponse);
			scan.close();
			JSONObject obj = new JSONObject(entireResponse);
			JSONObject count1 = obj.getJSONObject("ResultCounts");
			countcomm = count1.getInt("CommCount");
			counthome = count1.getInt("CommAllHomeCount");

			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Thread.sleep(5000);
		SearchTextbox.clear();
		SearchTextbox.sendKeys(searchObj.Zip);
		SearchButton.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));
		System.out.println(driver.getCurrentUrl());

		if (BrowseBaths.isEnabled()) {
			System.out.println("Bathroom is selected ");
		}

		else {

			System.out.println("Bathroom is not selected ");
		}
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		Thread.sleep(3000);
		String Count_CommHomes_bath = Count.getText();
		String[] parts2 = Count_CommHomes_bath.split(" ");
		String bath_part1 = parts2[0]; // 004
		System.out.println(bath_part1);
		int bath_resultcomm = Integer.parseInt(bath_part1);
		String bath_part2 = parts2[1]; // 034556
		String bath_part3 = parts2[2]; // 034556
		String bath_part4 = parts2[3]; // 034556
		int bath_resulthome = Integer.parseInt(bath_part4);
		String bath_part5 = parts2[4]; // 034556
		try {
			if (countcomm == bath_resultcomm) {

				System.out.println("community count matches for bath category and zip search");
			} else {
				System.out.println("community count does not matches for bath category and zip search");
			}
			Assert.assertTrue(countcomm == bath_resultcomm, "community count does not match for bath and zip search");
			if (counthome == bath_resulthome) {

				System.out.println("home count matches  for bath category and zip search");
			} else {
				System.out.println("home count does not matches for bath category and zip search");
			}
			Assert.assertTrue(counthome == bath_resulthome, "home count does not match for bath and zip search");
		} catch (Exception ee) {
			Assert.fail("Test Failed - Community count and home count does not matches" + ee.toString());
		}
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		Assert.assertTrue(driver.getCurrentUrl().contains("bathroom"),
				"bathroom room category verification with location change failed for city");

		BrowseBeds.click();
		try {

			// Passing API Url to check for api data so that we can match it
			URL url = new URL(
					"http://hlapi.newhomesource.com/Api/v2/search/homes?PartnerId=9383&marketid=269&pagesize=60&page=1&sortBy=Random&minImgWidth=400&minImgHeight=300&ExcludeDescription=1&ExcludeSummary=1&ExcludeBasiCommunities=1&ExcludeBasicListings=1&ExcludeSchoolDistricts=1&StateName=TX&sflow=500&sfhigh=10000&imgType=ELE%2CINT&imgCat=Bedroom&imgcolortype=TrueColor&Client=HomLuv");
			// Making connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			// Checking for the response
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("HTTP error code : " + conn.getResponseCode());
			}
			Scanner scan = new Scanner(url.openStream());
			String entireResponse = new String();
			while (scan.hasNext())
				entireResponse += scan.nextLine();
			System.out.println("Response : " + entireResponse);
			scan.close();
			JSONObject obj = new JSONObject(entireResponse);
			JSONObject count1 = obj.getJSONObject("ResultCounts");
			countcomm = count1.getInt("CommCount");
			counthome = count1.getInt("CommAllHomeCount");

			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		SearchTextbox.clear();
		SearchTextbox.sendKeys(searchObj.City + ", " + searchObj.Code + " " + searchObj.Market);// Market
																								// seacrh
																								// for
																								// bedroom
		SearchButton.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));
		System.out.println(driver.getCurrentUrl());
		if (BrowseBeds.isEnabled()) {
			System.out.println("Bedroom is selected ");
		}

		else {

			System.out.println("Bedroom is not selected ");
		}

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		Thread.sleep(3000);
		String Count_CommHomes_bed = Count.getText();
		String[] parts3 = Count_CommHomes_bed.split(" ");
		String bed_part1 = parts3[0]; // 004
		System.out.println(bed_part1);
		int bed_resultcomm = Integer.parseInt(bed_part1);
		String bed_part2 = parts3[1]; // 034556
		String bed_part3 = parts3[2]; // 034556
		String bed_part4 = parts3[3]; // 034556
		int bed_resulthome = Integer.parseInt(bed_part4);
		String bed_part5 = parts3[4]; // 034556
		try {
			if (countcomm == bed_resultcomm) {

				System.out.println("community count matches for bedroom category and market search");
			} else {
				System.out.println("community count does not matches for bedroom category and market search");
			}
			Assert.assertTrue(countcomm == bed_resultcomm,
					"community count does not match for bedroom and market search");
			if (counthome == bed_resulthome) {

				System.out.println("home count matches for bedroom category and market seacrh");
			} else {
				System.out.println("home count does not matches for bedroom category with market search");
			}
			Assert.assertTrue(counthome == bed_resulthome,
					"community count does not match for bedroom and market search");
		} catch (Exception ee) {
			Assert.fail(
					"Test Failed - Community count and home count does not matches for bedroom category with market search"
							+ ee.toString());
		}
		Assert.assertTrue(driver.getCurrentUrl().contains("bedroom"),
				"Bedroom room category verification with Market search failed");

		LivingArea.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		try {

			// Passing API Url to check for api data so that we can match it
			URL url = new URL(
					"http://hlapi.newhomesource.com/Api/v2/search/homes?PartnerId=9383&marketid=279&radius=25&OriginLat=29.883408&OriginLng=-96.277412&pagesize=60&page=1&sortBy=Random&minImgWidth=400&minImgHeight=300&ExcludeDescription=1&ExcludeSummary=1&ExcludeBasiCommunities=1&ExcludeBasicListings=1&ExcludeSchoolDistricts=1&StateName=TX&sflow=500&sfhigh=10000&county=Austin&imgType=ELE%2CINT&imgCat=Dining%2CGreatroom%2CGreatroom%20Combo%2CGreatroom%20Only%2CMedia%20Room%2CRecreation%20Room%2CStudy%20Room%2CWet%20Bar&imgcolortype=TrueColor&Client=HomLuv");
			// Making connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			// Checking for the response
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("HTTP error code : " + conn.getResponseCode());
			}
			Scanner scan = new Scanner(url.openStream());
			String entireResponse = new String();
			while (scan.hasNext())
				entireResponse += scan.nextLine();
			System.out.println("Response : " + entireResponse);
			scan.close();
			JSONObject obj = new JSONObject(entireResponse);
			JSONObject count1 = obj.getJSONObject("ResultCounts");
			countcomm = count1.getInt("CommCount");
			counthome = count1.getInt("CommAllHomeCount");

			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		SearchTextbox.clear();
		SearchTextbox.sendKeys(searchObj.City + " " + searchObj.County + ", " + searchObj.Code);
		SearchButton.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));

		System.out.println(driver.getCurrentUrl());
		if (LivingArea.isEnabled()) {
			System.out.println("LIVING is selected");
		}

		else {

			System.out.println("Living is not selected");
		}

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		Thread.sleep(3000);
		String Count_CommHomes_living = Count.getText();
		String[] parts4 = Count_CommHomes_living.split(" ");
		String living_part1 = parts4[0]; // 004
		System.out.println(living_part1);
		int living_resultcomm = Integer.parseInt(living_part1);
		String living_part2 = parts4[1]; // 034556
		String living_part3 = parts4[2]; // 034556
		String living_part4 = parts4[3]; // 034556
		int living_resulthome = Integer.parseInt(living_part4);
		String living_part5 = parts4[4]; // 034556
		try {
			if (countcomm == living_resultcomm) {

				System.out.println("community count matches");
			} else {
				System.out.println("community count does not matches");
			}
			Assert.assertTrue(countcomm == living_resultcomm,
					"community count does not match for living area market search");
			if (counthome == living_resulthome) {

				System.out.println("home count matches for living area category and market seacrh");
			} else {
				System.out.println("home count does not matches for living area category with market search");
			}
			Assert.assertTrue(counthome == living_resulthome,
					"community count does not match for living arae and market search");
		} catch (Exception ee) {
			Assert.fail(
					"Test Failed - Community count and home count does not matches for living area category with market search"
							+ ee.toString());
		}
		Assert.assertTrue(driver.getCurrentUrl().contains("living"),
				"Living room category verification with County with living area and county search failed");

		Interiors.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		try {

			// Passing API Url to check for api data so that we can match it
			URL url = new URL(
					"http://hlapi.newhomesource.com/Api/v2/search/homes?PartnerId=9383&marketid=269&radius=25&OriginLat=30.266899&OriginLng=-97.742798&pagesize=60&page=1&sortBy=Random&minImgWidth=400&minImgHeight=300&ExcludeDescription=1&ExcludeSummary=1&ExcludeBasiCommunities=1&ExcludeBasicListings=1&ExcludeSchoolDistricts=1&StateName=TX&sflow=500&sfhigh=10000&city=Austin&State=TX&imgType=ELE%2CINT&imgCat=Closet%2CDropzone%2CEmpty%2CFoyer%2CGarage%2CHallway%2CInterior%20Others%2CLaundry%2CStairs&imgcolortype=TrueColor&Client=HomLuv");
			// Making connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			// Checking for the response
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("HTTP error code : " + conn.getResponseCode());
			}
			Scanner scan = new Scanner(url.openStream());
			String entireResponse = new String();
			while (scan.hasNext())
				entireResponse += scan.nextLine();
			System.out.println("Response : " + entireResponse);
			scan.close();
			JSONObject obj = new JSONObject(entireResponse);
			JSONObject count1 = obj.getJSONObject("ResultCounts");
			countcomm = count1.getInt("CommCount");
			counthome = count1.getInt("CommAllHomeCount");

			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		SearchTextbox.clear();
		SearchTextbox.sendKeys(searchObj.City + ", " + searchObj.Code); // CITY
		SearchButton.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));

		System.out.println(driver.getCurrentUrl());
		if (Interiors.isEnabled()) {
			System.out.println("interiors is selected");
		}

		else {

			System.out.println("Interiors is not selected ");
		}
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		Thread.sleep(3000);
		String Count_CommHomes_interior = Count.getText();
		String[] parts5 = Count_CommHomes_interior.split(" ");
		String int_part1 = parts5[0]; // 004
		System.out.println(int_part1);
		int int_resultcomm = Integer.parseInt(int_part1);
		String int_part2 = parts5[1]; // 034556
		String int_part3 = parts5[2]; // 034556
		String int_part4 = parts5[3]; // 034556
		int int_resulthome = Integer.parseInt(int_part4);
		String int_part5 = parts3[4]; // 034556
		try {
			if (countcomm == int_resultcomm) {

				System.out.println("community count matches");
			} else {
				System.out.println("community count does not matches");
			}
			Assert.assertTrue(countcomm == int_resultcomm,
					"community count does not match for interior and city search");
			if (counthome == int_resulthome) {

				System.out.println("home count matches for interior category and market seacrh");
			} else {
				System.out.println("home count does not matches for interior category with market search");
			}
			Assert.assertTrue(counthome == int_resulthome,
					"community count does not match for interior and city search");
		} catch (Exception ee) {
			Assert.fail(
					"Test Failed - Community count and home count does not matches for interior category with city search"
							+ ee.toString());
		}
		Assert.assertTrue(driver.getCurrentUrl().contains("interior"),
				"interior room category verification with city search failed");

		Exteriors.click();
		try {
			// Passing API Url to check for api data so that we can match it
			URL url = new URL(
					"http://hlapi.newhomesource.com/Api/v2/search/homes?PartnerId=9383&marketid=269&radius=25&OriginLat=30.266899&OriginLng=-97.742798&pagesize=60&page=1&sortBy=Random&minImgWidth=400&minImgHeight=300&ExcludeDescription=1&ExcludeSummary=1&ExcludeBasiCommunities=1&ExcludeBasicListings=1&ExcludeSchoolDistricts=1&StateName=TX&sflow=500&sfhigh=10000&city=Austin&State=TX&imgType=ELE%2CINT&imgCat=Elevation%20Front%2CElevation%20Back%20Side%2CPatio%2CExterior%20Others%2CFront%20Porch%2Ctownhomes&imgcolortype=TrueColor&Client=HomLuv");
			// Making connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			// Checking for the response
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("HTTP error code : " + conn.getResponseCode());
			}
			Scanner scan = new Scanner(url.openStream());
			String entireResponse = new String();
			while (scan.hasNext())
				entireResponse += scan.nextLine();
			System.out.println("Response : " + entireResponse);
			scan.close();
			JSONObject obj = new JSONObject(entireResponse);
			JSONObject count1 = obj.getJSONObject("ResultCounts");
			countcomm = count1.getInt("CommCount");
			counthome = count1.getInt("CommAllHomeCount");

			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));

		System.out.println(driver.getCurrentUrl());
		if (Exteriors.isEnabled()) {
			System.out.println("exteriors is selected ");
		}

		else {

			System.out.println("Exteriors is not selected ");
		}
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		Thread.sleep(3000);
		String Count_CommHomes_ext = Count.getText();
		String[] parts6 = Count_CommHomes_ext.split(" ");
		String ext_part1 = parts6[0]; // 004
		System.out.println(ext_part1);
		int ext_resultcomm = Integer.parseInt(ext_part1);
		String ext_part2 = parts6[1]; // 034556
		String ext_part3 = parts6[2]; // 034556
		String ext_part4 = parts6[3]; // 034556
		int ext_resulthome = Integer.parseInt(ext_part4);
		System.out.println("home" + ext_resulthome);
		String ext_part5 = parts6[4]; // 034556
		try {
			if (countcomm == ext_resultcomm) {

				System.out.println("community count matches for exterior category");
			} else {
				System.out.println("community count does not matches for exterior category");
			}
			Assert.assertTrue(countcomm == ext_resultcomm,
					"community count does not match for exterior and city search");
			if (counthome == ext_resulthome) {

				System.out.println("home count matches for for exterior category and market seacrh");
			} else {
				System.out.println("home count does not matches for for exterior category with market search");
			}
			Assert.assertTrue(counthome == ext_resulthome,
					"community count does not match for exterior and city search");
		} catch (Exception ee) {
			Assert.fail(
					"Test Failed - Community count and home count does not matches for for exterior category with market search"
							+ ee.toString());
		}

		Assert.assertTrue(driver.getCurrentUrl().contains("new"),
				"Exterior room category verification with No search change failed");
	}

	// With location having no search result
	public void Browse_ZeroSearch(SearchModel searchObj) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		search(searchObj);
		System.out.println(driver.getCurrentUrl());
		Thread.sleep(5000);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		try {

			WebElement element = (new WebDriverWait(driver, 10)).until(
					ExpectedConditions.elementToBeClickable(By.xpath("(.//button[contains(text(),'Got It')])[2]")));
			element.click();
			Thread.sleep(5000);
		} catch (Exception e) {
			Error += "issue is :" + e.toString();
		}

		SearchTextbox.clear();
		SearchTextbox.sendKeys("78002");
		SearchButton.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		Thread.sleep(3000);
		String resultText = ZeroSearchRecord.getText();
		System.out.println("Text for location with no search result is as :- " + resultText);

	}

	public void filterapplied() throws Exception {

		try {

			// Passing API Url to check for api data so that we can match it
			// with website data
			URL url = new URL("http://api.newhomesource.com/api/V2/search/communities?partnerid=88&marketid=269");
			// Making connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			// Checking for the response
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("HTTP error code : " + conn.getResponseCode());
			}
			Scanner scan = new Scanner(url.openStream());
			String entireResponse = new String();
			while (scan.hasNext())
				entireResponse += scan.nextLine();
			System.out.println("Response : " + entireResponse);
			scan.close();
			JSONObject obj = new JSONObject(entireResponse);
			JSONObject count1 = obj.getJSONObject("ResultCounts");
			int count = count1.getInt("CommCount");
			int counthome = count1.getInt("HomeCount");
			System.out.println("Community count is.... : " + count);
			// Matching the communities and the homes count with response of API
			if (count == 414) {
				System.out.println("community count is correct");
			} else {
				System.out.println("Community count is not correct");
			}
			System.out.println("Home count is.... : " + counthome);
			if (count == 5573) {
				System.out.println("Home count is correct");
			} else {
				System.out.println("Home count is not correct");
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

	// Testing Basic Filters and matching it with API
	public void Browse_BasicFilters(FiltersModel filterObj, SearchModel searchObj)
			throws InterruptedException, JSONException {
		int countcomm = 0;
		int counthome = 0;
		try {

			// Passing API Url to check for api data so that we can match it
			URL url = new URL(
					"http://hlapi.newhomesource.com/Api/v2/search/homes?PartnerId=9383&marketid=269&radius=25&OriginLat=30.266899&OriginLng=-97.742798&bed=1&bath=1&pagesize=60&page=1&sortBy=Random&minImgWidth=400&minImgHeight=300&ExcludeDescription=1&ExcludeSummary=1&ExcludeBasiCommunities=1&ExcludeBasicListings=1&ExcludeSchoolDistricts=1&StateName=TX&prlow=110000&sflow=800&sfhigh=10000&city=Austin&State=TX&imgType=ELE%2CINT&imgCat=Breakfast%2CKitchen%2CPantry&imgcolortype=TrueColor&Client=HomLuv");
			// Making connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			// Checking for the response
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("HTTP error code : " + conn.getResponseCode());
			}
			Scanner scan = new Scanner(url.openStream());
			String entireResponse = new String();
			while (scan.hasNext())
				entireResponse += scan.nextLine();
			System.out.println("Response : " + entireResponse);
			scan.close();
			JSONObject obj = new JSONObject(entireResponse);
			JSONObject count1 = obj.getJSONObject("ResultCounts");
			countcomm = count1.getInt("CommCount");
			counthome = count1.getInt("CommAllHomeCount");

			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		WebDriverWait wait = new WebDriverWait(driver, 10);
		search(searchObj);
		Thread.sleep(5000);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		try {
			WebElement element = (new WebDriverWait(driver, 10)).until(
					ExpectedConditions.elementToBeClickable(By.xpath("(.//button[contains(text(),'Got It')])[2]")));
			element.click();
			Thread.sleep(5000);
		} catch (Exception e) {
			Error += "issue is :" + e.toString();
		}
		Actions action = new Actions(driver);
		action.click(Priceslider).build().perform();
		Thread.sleep(1000);
		for (int i = 0; i < 10; i++) {
			action.sendKeys(Keys.ARROW_RIGHT).build().perform();

		}
		action.click(SqFtslider).build().perform();
		Thread.sleep(1000);
		for (int i = 0; i < 3; i++) {
			action.sendKeys(Keys.ARROW_RIGHT).build().perform();
			Thread.sleep(200);
		}
		Beds.click();
		Bath.click();
		Thread.sleep(2000);
		WebElement grid = driver.findElement(By.xpath(".//*[@id='listdata']/div[4]"));
		wait.until(ExpectedConditions.visibilityOf(grid));
		String count = Count.getText();
		System.out.println("Count of community and homes after filer applied is " + count);
		Thread.sleep(5000);
		String[] parts = count.split(" ");
		String part1 = parts[0]; // 004
		System.out.println(part1);
		int resultcomm = Integer.parseInt(part1);
		String part2 = parts[1]; // 034556
		String part3 = parts[2]; // 034556
		String part4 = parts[3]; // 034556
		int resulthome = Integer.parseInt(part4);
		String part5 = parts[4]; // 034556
		try {
			if (countcomm == resultcomm) {

				System.out.println("community count matches");
			} else {
				System.out.println("community count does not matches");
			}

			if (counthome == resulthome) {

				System.out.println("home count matches");
			} else {
				System.out.println("home count does not matches");
			}
		} catch (Exception ee) {
			Assert.fail("Test Failed - Community count and home count does not matches" + ee.toString());
		}
	}
}
