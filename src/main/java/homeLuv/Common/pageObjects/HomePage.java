
package homeLuv.Common.pageObjects;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Locators.HomePageLocators;
import homeLuv.Models.LoginModel;
import homeLuv.Models.SearchModel;
import homeLuv.Models.UserModel;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class HomePage extends BasePageObject {

	public HomePageLocators home;
	@FindBy(xpath = ".//*[@id='headerContent_']/ul/li[1]")
	protected WebElement Signuplink; //

	@FindBy(xpath = ".//*[@id='headerContent_']/ul/li[2]")
	protected WebElement SignInlink;

	@FindBy(xpath = ".//*[@id='signup-modal-content']/div[2]/form/div[1]/div/input")
	public WebElement firstName;

	@FindBy(xpath = ".//*[@id='signup-modal-content']/div[2]/form/div[2]/div/input")
	public WebElement lastName;

	@FindBy(xpath = ".//*[@id='signup-modal-content']/div[2]/form/div[3]/div/input")
	public WebElement Email;

	@FindBy(xpath = ".//*[@id='signup-modal-content']/div[2]/form/div[4]/div[1]/div/input")
	public WebElement Password;

	@FindBy(xpath = ".//*[@id='zip']")
	public WebElement zipCode;

	@FindBy(xpath = ".//*[@id='mainDiv']/section/div[1]/div[2]")
	public WebElement RoomCategories;

	@FindBy(xpath = ".//*[@id='mainDiv']/section/div[1]/div[2]/ul/li[1]/div[1]")
	public WebElement HomeKitchen;

	@FindBy(xpath = ".//*[@id='signup-modal-content']/div[2]/form/div[8]/button[2]")
	public WebElement SubmitCreateAccount;

	@FindBy(id = "searchTextbox")
	public WebElement SearchTextbox;

	@FindBy(xpath = ".//*[@id='searchbutton']")
	public WebElement SearchButton;

	@FindBy(xpath = ".//*[@id='listdata']/div[2]/img")
	public WebElement Loading;

	// https://resources.homluv.com/Content/image/loader.gif

	@FindBy(xpath = ".//*[@id='imageCatSlider']")
	public WebElement CategorySlider;

	@FindBy(xpath = ".//*[@id='imageCatSlider']/li[1]/a")
	public WebElement BrowseKitchen;

	@FindBy(xpath = ".//*[@id='myluvWrap']/header/div/div[1]/a/img")
	public WebElement HomeImage;

	@FindBy(xpath = ".//*[@id='mainDiv']/section/div[1]/div[2]/ul/li[2]/div[1]")
	public WebElement HomeBathrooms;

	@FindBy(xpath = ".//*[@id='imageCatSlider']/li[2]/a")
	public WebElement BrowseBaths;

	@FindBy(id = "forgot-password-modal-content")
	public WebElement ForgotPwdModal;

	@FindBy(css = "#FPModal")
	public WebElement ForgotPwd;

	@FindBy(xpath = ".//*[@id='forgot-password-modal-content']/form/div[1]/div[2]/div/input")
	public WebElement FP_Email;

	@FindBy(xpath = ".//*[@id='forgot-password-modal-content']/form/div[2]/button[1]")
	public WebElement ResetPwdBtn;

	@FindBy(xpath = ".//*[@id='login-modal-content']")
	public WebElement LoginModal;

	@FindBy(css = "#commonFooter_>p")
	public WebElement FooterTxt;

	@FindBy(xpath = ".//*[@id='mainDiv']/section/div[1]/div")
	public WebElement HeroImage;

	@FindBy(xpath = ".//*[@id='test']/p")
	public WebElement SearchError;

	@FindBy(xpath = ".//*[@id='headerContent_']/ul/li[3]")
	public WebElement SignInDrop;

	@FindBy(xpath = ".//*[@id='headerContent_']/ul/li[3]/ul/li[1]/a")
	public WebElement myAccount;

	@FindBy(xpath = ".//*[@id='MyCollectionEducation']/div/div/div[3]/button")
	public WebElement MyColBtn;

	@FindBy(id = "user-name")
	public WebElement name;

	@FindBy(id = "last-name")
	public WebElement lastname;

	@FindBy(id = "profileEmail")
	public WebElement MA_email;

	@FindBy(xpath = ".//*[@id='mainDiv']/section/div/div[2]/form/div[1]/div[2]/div[7]/div/span")
	public WebElement UploadImage;

	@FindBy(xpath = ".//*[@id='mainDiv']/section/div/div[2]/form/div[2]/div[2]/button")
	public WebElement UpdateAccount;

	@FindBy(xpath = ".//*[@id='leadSubmitted']/div/div/div[3]/button")
	public WebElement MyAccountbtn;

	String Error = "";

	// Parameterized Constructor
	public HomePage(WebDriver driver) {
		super(driver);
		// test
		// test1
		// AjaxElementLocatorFactory factory = new
		// AjaxElementLocatorFactory(driver, 10);
		// PageFactory.initElements(driver, this.home);
	}

	public void Login(LoginModel loginObj) {
		login(loginObj);
	}

	public void Signup_Login(LoginModel loginObj) {
		Signup_login(loginObj);
	}

	// Validating all Links,URLs and Images of Home Page

	public void HomePageLinksAndVideos() throws InterruptedException {

		LinksImagesVideosVerification.validateAllLinksOnPage(driver);
		LinksImagesVideosVerification.validateInvalidImages(driver);
	}

	// Methos for create Account
	public void CreateAccount(UserModel userObj) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='myluvWrap']/header/div")));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='headerContent_']/ul/li[1]")));
		Thread.sleep(5000);
		Signuplink.click();

		try {
			firstName.sendKeys(userObj.FirstName);
			lastName.sendKeys(userObj.LastName);
			Thread.sleep(3000);
			Random randomGenerator = new Random();
			int randomInt = randomGenerator.nextInt(1000);

			Email.sendKeys("RuchikaQA" + randomInt + "@gmail.com");
			Password.sendKeys(userObj.Password);
			zipCode.sendKeys(userObj.ZipCode);
			Thread.sleep(3000);

			SubmitCreateAccount.click();

			Thread.sleep(3000);

		} catch (Exception e) {
			Assert.fail("Test Failed - unable to create an account!!" + e.toString());
		}
	}

	// Searching from HomePage
	public void Home_Search(SearchModel searchObj) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		SearchTextbox.clear();
		HeroImage.click();
		String Placeholder = SearchTextbox.getAttribute("placeholder");
		System.out.println("Placeholder text is " + Placeholder);
		SearchTextbox.sendKeys("aaaaaa");
		SearchButton.click();
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
		}

		String errormsg = SearchError.getText();
		System.out.println("Error text is " + errormsg);

		SearchTextbox.clear();

		SearchTextbox.sendKeys(searchObj.City + ", " + searchObj.Code);
		SearchButton.click();
		Thread.sleep(2000);
		System.out.println(driver.getCurrentUrl());
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));

		if (BrowseKitchen.isEnabled()) {
			System.out.println("kitchen is selected by default");
		}

		else {

			System.out.println("kitchen is not selected by default");
		}

		HomeImage.click();
		wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(".//*[@id='searchTextbox']"))));
		SearchTextbox.clear();
		SearchTextbox.sendKeys(searchObj.Zip);
		SearchButton.click();
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));
		System.out.println(driver.getCurrentUrl());
		if (BrowseKitchen.isEnabled()) {
			System.out.println("kitchen is selected by default");
		}

		else {

			System.out.println("kitchen is not selected by default");
		}
		HomeImage.click();
		wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(".//*[@id='searchTextbox']"))));
		SearchTextbox.clear();
		SearchTextbox.sendKeys(searchObj.City + ", " + searchObj.Code + " " + searchObj.Market); // market
		SearchButton.click();
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));
		System.out.println(driver.getCurrentUrl());

		if (BrowseKitchen.isEnabled()) {
			System.out.println("kitchen is selected by default");
		}

		else {

			System.out.println("kitchen is not selected by default");
		}
		HomeImage.click();
		wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(".//*[@id='searchTextbox']"))));
		SearchTextbox.clear();
		// country
		SearchButton.click();
		System.out.println(driver.getCurrentUrl());
		wait.until(ExpectedConditions.visibilityOf(CategorySlider));
		System.out.println(driver.getCurrentUrl());

		if (BrowseKitchen.isEnabled()) {
			System.out.println("kitchen is selected by default");
		}

		else {

			System.out.println("kitchen is not selected by default");
		}
	}

	public void Home_UI_Compare() throws InterruptedException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 10);

		BufferedImage expectedImage = ImageIO.read(new File("src\\test\\resources\\compare\\compare.png"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='myluvWrap']/header/div")));
		wait.until(ExpectedConditions.visibilityOf(RoomCategories));
		Thread.sleep(3000);
		// screenshot();

		Screenshot logoImageScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
				.takeScreenshot(driver);
		BufferedImage actualImage = logoImageScreenshot.getImage();

		ImageDiffer imgDiff = new ImageDiffer();
		ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);
		BufferedImage diffImage = diff.getDiffImage();
		diff = imgDiff.makeDiff(diffImage, actualImage);

		if (diff.hasDiff()) {
			System.out.println(" diff Image & actual image are not same" + diff.getDiffImage().getData());
		} else {
			System.out.println("Images are same");
		}

		diff = imgDiff.makeDiff(diffImage, expectedImage);

		if (diff.hasDiff()) {
			System.out.println(" diff Image & xpected image are not same");
		} else {
			System.out.println(" expected & Diff Images are same");
		}

	}

	public void Home_ForgotPwd(LoginModel loginObj) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		SignInlink.click();
		wait.until(ExpectedConditions.visibilityOf(LoginModal));
		ForgotPwd.click();
		wait.until(ExpectedConditions.visibilityOf(ForgotPwdModal));
		FP_Email.sendKeys(loginObj.EmailAddress);
		ResetPwdBtn.click();

	}

	// My Account test case
	public void Home_MyAccount(LoginModel loginObj) throws InterruptedException {

		login(loginObj);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		Thread.sleep(5000);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		if (MyColBtn.isDisplayed()) {
			MyColBtn.click();
		}

		try {
			Thread.sleep(5000);
		} catch (Exception e)

		{
			Error += "issue is :" + e.toString();

		}

		SignInDrop.click();
		Thread.sleep(3000);
		myAccount.click();
		Thread.sleep(5000);
		String Url = driver.getCurrentUrl();
		System.out.println(" My Account Url IS " + Url);

		String FirstName = name.getAttribute("value");
		String LastName = lastname.getAttribute("value");
		String Emailid = MA_email.getAttribute("value");
		System.out.println("My account detail is-" + FirstName + ", " + LastName + ", " + Emailid);
		name.clear();
		name.sendKeys("test");

		try {
			Thread.sleep(4000);
		} catch (Exception e) {
		}

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", UpdateAccount);
		UpdateAccount.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		MyAccountbtn.click();

		System.out.println(" firstname edited is now " + name.getAttribute("value"));
		name.clear();
		name.sendKeys("ruchika");

		try {
			Thread.sleep(4000);
		} catch (Exception e) {
		}

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", UpdateAccount);
		UpdateAccount.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='listdata']/div[2]/img")));
		MyAccountbtn.click();
		screenshot();
		Assert.assertTrue(driver.getCurrentUrl().contains("myaccount"), "My account verification failed");
	}
}
