package homeLuv.Common.Utilities;

public class Constants {
	public static final String SignInTestCase = "Sign in";
	public static final String HomePageLinksAndVideosTestCase = "Links and videos testing";
	public static final String CreateAccountTestCase = "Create Account testing";
	public static final String Home_Search = "Home Search with room categories";
	public static final String Home_UI_CompareTestCase = "UI of home page testing";
	public static final String Home_ForgotPwdTestCase = "Forgot PWD testing";
	public static final String Footers = "Testing footers links";
	public static final String SignupSignInTestCase = "Login using signup modaL";
	public static final String Home_MyAccountTestCase = "Testing My Account";
	public static final String Browse_RoomCategory = "Testing Browse page room category";
	public static final String Browselocation_Search = "Browse search location";
	public static final String Browse_ZeroSearch_Search = "Testing location not returning any search location";
	public static final String Browse_BasicFilters_Search = "Basic filters testing";

}
