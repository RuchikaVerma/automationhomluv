package homeLuv.Models;

public class UserModel {
	public UserModel() {

	}

	public UserModel(String firstName, String lastName, String emailAddress, String password, String zipCode) {

		this.FirstName = firstName;
		this.LastName = lastName;
		this.EmailAddress = emailAddress;
		this.Password = password;
		this.ZipCode = zipCode;

	}

	public String EmailAddress;
	public String FirstName;
	public String LastName;
	public String ZipCode;
	public String Password;

}
