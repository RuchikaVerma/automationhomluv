package homeLuv.Models;

public class SearchModel {
	public SearchModel(String city, String code, String zip, String market, String county) {
		this.City = city;
		this.Code = code;
		this.Zip = zip;
		this.Market = market;
		this.County = county;

	}

	public String City;
	public String Code;
	public String Zip;
	public String Market;
	public String County;

}
