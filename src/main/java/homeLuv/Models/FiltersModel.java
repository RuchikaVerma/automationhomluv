package homeLuv.Models;

public class FiltersModel {
	public FiltersModel() {

	}

	public FiltersModel(String minprice, String maxprice, String minsqft, String maxsqft, String beds, String bath,
			String buildername, String communities, String hometype, String homestatus, String specialoffer,
			String garages, String livingareas, String stories, String masterbedroom) {
		this.MinPrice = minprice;
		this.MaxPrice = maxprice;
		this.MinSqFt = minsqft;
		this.MaxSqFt = maxsqft;
		this.Beds = beds;
		this.Bath = bath;
		this.BuilderName = buildername;
		this.Communities = communities;
		this.HomeType = hometype;
		this.HomeStatus = homestatus;
		this.SpecialOffer = specialoffer;
		this.Garages = garages;
		this.LivingAreas = livingareas;
		this.Stories = stories;
		this.MasterBedroom = masterbedroom;

	}

	public String MinPrice;
	public String MaxPrice;
	public String MinSqFt;
	public String MaxSqFt;
	public String Beds;
	public String Bath;
	public String BuilderName;
	public String Communities;
	public String HomeType;
	public String HomeStatus;
	public String SpecialOffer;
	public String Garages;
	public String LivingAreas;
	public String Stories;
	public String MasterBedroom;

}
