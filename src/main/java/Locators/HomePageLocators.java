
package Locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePageLocators {

	@FindBy(xpath = ".//*[@id='signinbutton']")
	public WebElement SigninLink; // signin link from header
	@FindBy(xpath = ".//*[@id='login-modal-content']/div[2]/form/div[1]/div/input")
	public WebElement emailField;
	@FindBy(xpath = ".//*[@id='loginModal") // SignIn modal link from header
	public WebElement loginButton; // signin link
	@FindBy(xpath = ".//*[@id='login-modal-content']/div[2]/form/div[2]/div/input") // Password
																					// Textbox
	public WebElement passwordfield;

	@FindBy(id = ".//*[@id='login-modal-content']/div[2]/form/div[4]/button[2]")
	public WebElement submitButton; // sigin btn

}